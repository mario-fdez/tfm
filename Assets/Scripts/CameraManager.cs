﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CameraManager : MonoBehaviour
{
    [SerializeField] private float left = -0.2F;
    [SerializeField] private float right = 0.2F;
    [SerializeField] private float top = 0.2F;
    [SerializeField] private float bottom = -0.2F;

    [SerializeField] private float offset = 5;

    private float point;

    private Camera cam;

    private void Awake()
    {
        cam = Camera.main;
        point = -cam.transform.position.z - 5;// + offset;
    }

    void LateUpdate()
    {
        if (cam == null) cam = Camera.main;
        point = -cam.transform.position.z - 5;
        Matrix4x4 m = ProyectionFocusPoint(left - transform.position.x, right - transform.position.x, bottom - transform.position.y, top - transform.position.y, cam.nearClipPlane, cam.farClipPlane);
        cam.projectionMatrix = m;
    }

    private Matrix4x4 ProyectionFocusPoint(float left, float right, float bottom, float top, float near, float far)
    {
        float x = 2.0F * point / (right - left);
        float y = 2.0F * point / (top - bottom);
        float a = (right + left) / (right - left);
        float b = (top + bottom) / (top - bottom);
        float c = -(far + near) / (far - near);
        float d = -(2.0F * far * near) / (far - near);
        float e = -1.0F;
        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = x;
        m[0, 1] = 0;
        m[0, 2] = a;
        m[0, 3] = 0;
        m[1, 0] = 0;
        m[1, 1] = y;
        m[1, 2] = b;
        m[1, 3] = 0;
        m[2, 0] = 0;
        m[2, 1] = 0;
        m[2, 2] = c;
        m[2, 3] = d;
        m[3, 0] = 0;
        m[3, 1] = 0;
        m[3, 2] = e;
        m[3, 3] = 0;
        return m;
    }
}