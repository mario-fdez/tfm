﻿#if UNITY_ANDROID && !UNITY_EDITOR
#define ASTRA_UNITY_ANDROID_NATIVE
#endif

using Astra;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Debug = UnityEngine.Debug;

public class CustomAstra : Singleton<AstraManager>
{
    private Astra.StreamSet _streamSet;
    private Astra.StreamReader _readerBody;

    private BodyStream _bodyStream;
    //Get body stream
    public BodyStream BodyStream
    {
        get
        {
            return _bodyStream;
        }
    }

    bool _isBodyOn = true;
    //Get and set body stream on
    public bool IsBodyOn
    {
        get
        {
            return _isBodyOn;
        }
        set
        {
            _isBodyOn = value;
        }
    }

    private Body[] _bodies = { };
    //Get bodies
    public Body[] Bodies
    {
        get
        {
            return _bodies;
        }
    }

    private long _lastBodyFrameIndex = -1;

    private int _lastWidth = 0;
    private int _lastHeight = 0;
    private short[] _buffer;
    private int _frameCount = 0;
    private int _initializeCount = 500;

    private bool _areStreamsInitialized = false;
    //Get whether initialize
    public bool Initialized
    {
        get
        {
            return _areStreamsInitialized;
        }
    }

    [SerializeField]
    private string _license = "";
    //Get and set license
    public string License
    {
        get
        {
            return _license;
        }
        set
        {
            _license = value;
        }
    }

    private void Start()
    {
        AstraUnityContext.Instance.Initializing += OnAstraInitializing;
        AstraUnityContext.Instance.Terminating += OnAstraTerminating;

        Debug.Log("AstraUnityContext initialize");
        AstraUnityContext.Instance.Initialize();
    }

    private void OnAstraInitializing(object sender, AstraInitializingEventArgs e)
    {
        if (!string.IsNullOrEmpty(_license))
        {
            Debug.Log("Set license: " + _license);
            Astra.BodyTracking.SetLicense(_license);
        }

#if ASTRA_UNITY_ANDROID_NATIVE
        // if (AutoRequestAndroidUsbPermission)
        // {
            Debug.Log("Auto-requesting usb device access.");
            AstraUnityContext.Instance.RequestUsbDeviceAccessFromAndroid();
        // }
#endif
        Debug.Log("Initialize streams");
        InitializeStreams();
    }

    private void InitializeStreams()
    {
        try
        {
            AstraUnityContext.Instance.WaitForUpdate(AstraBackgroundUpdater.WaitIndefinitely);

            _streamSet = Astra.StreamSet.Open();
            _readerBody = _streamSet.CreateReader();    

            _bodyStream = _readerBody.GetStream<BodyStream>();

            _areStreamsInitialized = true;

            Debug.Log("Stream initialize success");
        }
        catch (System.Exception e)
        {
            Debug.Log("Couldn't initialize streams: " + e.ToString());
            UninitializeStreams();

            if (_initializeCount > 0)
            {
                _initializeCount--;
            }
            else
            {
                Debug.Log("Initialize failed");
            }
        }
    }

    private void OnAstraTerminating(object sender, AstraTerminatingEventArgs e)
    {
        Debug.Log("Astra is tearing down");
        UninitializeStreams();
    }

    private void UninitializeStreams()
    {
        AstraUnityContext.Instance.WaitForUpdate(AstraBackgroundUpdater.WaitIndefinitely);

        Debug.Log("Uninitializing streams");
        if (_readerBody != null)
        {
            _readerBody.Dispose();
            _readerBody = null;
        }
        if (_streamSet != null)
        {
            _streamSet.Dispose();
            _streamSet = null;
        }
    }


    private void CheckBodyReader()
    {
        // Assumes AstraUnityContext.Instance.IsUpdateAsyncComplete is already true

        ReaderFrame frame;
        if (_readerBody.TryOpenFrame(0, out frame))
        {
            using (frame)
            {
                BodyFrame bodyFrame = frame.GetFrame<BodyFrame>();

                if (bodyFrame != null)
                {
                    if (_lastBodyFrameIndex != bodyFrame.FrameIndex)
                    {
                        _lastBodyFrameIndex = bodyFrame.FrameIndex;

                        UpdateBody(bodyFrame);
                    }
                }
            }
        }
    }

    private void CheckForNewFrames()
    {
        if (AstraUnityContext.Instance.WaitForUpdate(5) && AstraUnityContext.Instance.IsUpdateAsyncComplete)
        {
            // Inside this block until UpdateAsync() call below, we can use the Astra API safely
            CheckBodyReader();

            _frameCount++;

        }

        if (!AstraUnityContext.Instance.IsUpdateRequested)
        {
            UpdateStreamStartStop();
            // After calling UpdateAsync() the Astra API will be called from a background thread
            AstraUnityContext.Instance.UpdateAsync(delegate { return true; });
        }
    }

    void PrintBody(Astra.BodyFrame bodyFrame)
    {
        if (bodyFrame != null)
        {
            Body[] bodies = { };
            bodyFrame.CopyBodyData(ref bodies);
            foreach (Body body in bodies)
            {
                Astra.Joint headJoint = body.Joints[(int)JointType.Head];

                Debug.Log("Body " + body.Id + " COM " + body.CenterOfMass +
                    " Head Depth: " + headJoint.DepthPosition.X + "," + headJoint.DepthPosition.Y +
                    " World: " + headJoint.WorldPosition.X + "," + headJoint.WorldPosition.Y + "," + headJoint.WorldPosition.Z +
                    " Status: " + headJoint.Status.ToString());
            }
        }
    }

    void PrintDepth(Astra.DepthFrame depthFrame,
                    Astra.CoordinateMapper mapper)
    {
        if (depthFrame != null)
        {
            int width = depthFrame.Width;
            int height = depthFrame.Height;
            long frameIndex = depthFrame.FrameIndex;

            //determine if buffer needs to be reallocated
            if (width != _lastWidth || height != _lastHeight)
            {
                _buffer = new short[width * height];
                _lastWidth = width;
                _lastHeight = height;
            }
            depthFrame.CopyData(ref _buffer);

            int index = (int)((width * (height / 2.0f)) + (width / 2.0f));
            short middleDepth = _buffer[index];

            Vector3D worldPoint = mapper.MapDepthPointToWorldSpace(new Vector3D(width / 2.0f, height / 2.0f, middleDepth));
            Vector3D depthPoint = mapper.MapWorldPointToDepthSpace(worldPoint);

            Debug.Log("depth frameIndex: " + frameIndex
                      + " width: " + width
                      + " height: " + height
                      + " middleDepth: " + middleDepth
                      + " wX: " + worldPoint.X
                      + " wY: " + worldPoint.Y
                      + " wZ: " + worldPoint.Z
                      + " dX: " + depthPoint.X
                      + " dY: " + depthPoint.Y
                      + " dZ: " + depthPoint.Z + " frameCount: " + _frameCount);
        }
    }

    private void UpdateStreamStartStop()
    {
        if (_bodyStream != null)
        {
            if (_isBodyOn)
            {
                _bodyStream.Start();
            }
            else
            {
                _bodyStream.Stop();
            }
        } 
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_areStreamsInitialized)
        {
            InitializeStreams();
        }

        if (_areStreamsInitialized)
        {
            CheckForNewFrames();
        }
    }

    void OnDestroy()
    {
        AstraUnityContext.Instance.WaitForUpdate(AstraBackgroundUpdater.WaitIndefinitely);
 

        if (_bodyStream != null)
        {
            _bodyStream.Stop();
        }
   
        UninitializeStreams();

        AstraUnityContext.Instance.Initializing -= OnAstraInitializing;
        AstraUnityContext.Instance.Terminating -= OnAstraTerminating;

        Debug.Log("AstraUnityContext terminate");
        AstraUnityContext.Instance.Terminate();
    }

    private void OnApplicationQuit()
    {
        Debug.Log("Handling OnApplicationQuit");
        AstraUnityContext.Instance.Terminate();
    }

    public void UpdateBody(BodyFrame bodyFrame)
    {
        if (bodyFrame == null)
        {
            return;
        }
        bodyFrame.CopyBodyData(ref _bodies);
    }
}
