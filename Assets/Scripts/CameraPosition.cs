﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    private void Start()
    {
#if UNITY_ANDROID
        Input.gyro.enabled = true;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
    }

    private void Update()
    {
#if UNITY_ANDROID
        float vectorRight = GetAngleByDeviceAxis(Vector3.right);
        float vectorTop = GetAngleByDeviceAxis(Vector3.up);
        float vectorForward = GetAngleByDeviceAxis(Vector3.forward);

        float roll = (vectorRight > 180) ? (vectorRight - 360) : vectorRight;
        float pitch = (vectorTop > 180) ? (vectorTop - 360) : vectorTop;

        Vector3 newPose = new Vector3(pitch * 0.15f, -roll * 0.15f, -10);
        transform.position = Vector3.LerpUnclamped(transform.position, newPose, speed * Time.deltaTime);
#endif
    }

    private float GetAngleByDeviceAxis(Vector3 axis)
    {
        Quaternion deviceRotation = Input.gyro.attitude;
        Quaternion eliminationOfOthers = Quaternion.Inverse(
            Quaternion.FromToRotation(axis, deviceRotation * axis)
        );
        Vector3 filteredEuler = (eliminationOfOthers * deviceRotation).eulerAngles;

        float result = filteredEuler.z;
        if (axis == Vector3.up)
        {
            result = filteredEuler.y;
        }
        if (axis == Vector3.right)
        {
            // incorporate different euler representations.
            result = (filteredEuler.y > 90 && filteredEuler.y < 270) ? 180 - filteredEuler.x : filteredEuler.x;
        }
        return result;
    }
}
