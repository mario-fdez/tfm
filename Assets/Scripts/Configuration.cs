﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Configuration : MonoBehaviour
{
    public static bool isInverted;
    public static float heightOffset;
    public static float widthOffset;

    private void Awake()
    {
        widthOffset = PlayerPrefs.GetFloat("offset", 100);
        heightOffset = PlayerPrefs.GetFloat("offset2", 1);
        isInverted = PlayerPrefs.GetInt("inverted") == 0 ? false : true;
    }

    public void UpdateOffset(UnityEngine.UI.Slider slider)
    {
        widthOffset = slider.value;
        PlayerPrefs.SetFloat("offset", widthOffset);
    }

    public void UpdateSecondOffset(UnityEngine.UI.Slider slider)
    {
        heightOffset = slider.value;
        PlayerPrefs.SetFloat("offset2", heightOffset);

    }
    public void UpdatEthirdOffset(UnityEngine.UI.Toggle toggle)
    {
        isInverted = toggle.isOn;
        PlayerPrefs.SetInt("inverted", (isInverted) ? 1 : 0);

    }

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index, LoadSceneMode.Single);
    }

    /*
    private IEnumerator ConfigurationRoutine()
    {
        bool canContinue = false;

        heightSlider.gameObject.SetActive(true);

        do
        {
            if(Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                canContinue = true;
            }

            yield return null;
        }
        while (!canContinue);

        heightOffset = heightSlider.value;
        heightSlider.gameObject.SetActive(false);

        widthSlider.gameObject.SetActive(true);

        do
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                canContinue = true;
            }

            yield return null;
        }
        while (!canContinue);

        widthOffset = widthSlider.value;
        widthSlider.gameObject.SetActive(false);
    }*/
}
