﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BodyView : MonoBehaviour
{
    private Dictionary<Astra.JointType, GameObject> jointGOs;
    //private LineRenderer[] jointLines;
    private float offset = 100;
    private float secondOffset = 1;
    private float thirdOffset;
    private Transform cam;

    // Use this for initialization
    void Start()
    {
        offset = PlayerPrefs.GetFloat("offset", 100);
        secondOffset = PlayerPrefs.GetFloat("offset2",1);
        thirdOffset = PlayerPrefs.GetFloat("offset3");

        cam = Camera.main.transform;
        StreamViewModel viewModel = StreamViewModel.Instance;
        viewModel.bodyStream.onValueChanged += OnBodyStreamChanged;

        jointGOs = new Dictionary<Astra.JointType, GameObject>();
        for (int i = 0; i < 19; ++i)
        {
            var jointGO = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            jointGO.name = ((Astra.JointType)i).ToString();
            //jointGO.transform.localScale = new Vector3(100f, 100f, 100f);
            jointGO.transform.SetParent(transform, false);
            jointGO.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
            jointGO.SetActive(false);
            jointGOs.Add((Astra.JointType)i, jointGO);
        }
    }

    public void UpdateOffset(UnityEngine.UI.Slider slider)
    {
        offset = slider.value;
        PlayerPrefs.SetFloat("offset", offset);
    }

    public void UpdateSecondOffset(UnityEngine.UI.Slider slider)
    {
        secondOffset = slider.value;
        PlayerPrefs.SetFloat("offset2", secondOffset);

    }
    public void UpdatEthirdOffset(UnityEngine.UI.Slider slider)
    {
        thirdOffset = slider.value;
        PlayerPrefs.SetFloat("offset3", thirdOffset);

    }

    private void OnBodyStreamChanged(bool value)
    {
        gameObject.SetActive(value);
    }

    // Update is called once per frame
    void Update()
    {
        if (!AstraManager.Instance.Initialized || !AstraManager.Instance.IsBodyOn)
        {
            return;
        }
        var bodies = AstraManager.Instance.Bodies;
        if (bodies == null)
        {
            return;
        }
        foreach (var body in bodies)
        {
            var joints = body.Joints;
            if (joints != null)
            {
                foreach (var joint in joints)
                {
                    if (joint.Status == Astra.JointStatus.Tracked)
                    {
                        var jointPos = joint.WorldPosition;
                        jointGOs[joint.Type].transform.localPosition = new Vector3(jointPos.X, jointPos.Y, jointPos.Z);
                        jointGOs[joint.Type].SetActive(true);

                        if(joint.Type == Astra.JointType.Head)
                        {
                            UpdateCameraPosition(jointPos.X, jointPos.Y, jointPos.Z);
                        }
                    }
                    else
                    {
                        jointGOs[joint.Type].SetActive(false);
                    }
                }

                break;
            }
        }
    }

    private void UpdateCameraPosition(float x, float y, float z)
    {
        if(Configuration.isInverted)
        {
            cam.position = new Vector3(x / Configuration.widthOffset, (z / Configuration.widthOffset) + Configuration.heightOffset, -15);// (z * secondOffset * 0.01f));
        }
        else
        {
            cam.position = new Vector3(x / Configuration.widthOffset, (y / Configuration.widthOffset) + Configuration.heightOffset, -15);// (z * secondOffset * 0.01f));
        }
    }
}
